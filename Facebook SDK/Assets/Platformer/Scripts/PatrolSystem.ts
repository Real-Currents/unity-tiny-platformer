
namespace game {

    /** New System */
    export class PatrolSystem extends ut.ComponentSystem {

        OnUpdate(): void {
            this.world.forEach([ut.Entity, game.Input, game.Movement, game.Patrol, ut.Core2D.TransformLocalPosition, game.Ai],
                (entity, input, movement, patrol, transform, Ai) => {
                    switch (patrol.Direction) {
                        case 0:
                            let scale = this.world.getComponentData(entity, ut.Core2D.TransformLocalScale);
                            if(scale.scale.x == -1 && GameService.CanWalkLeft(this.world, transform)){
                                InputService.Move(input, movement, new Vector2(1, input.Axis.y));
                                patrol.Direction = -1;
                            }else if(scale.scale.x == 1 && GameService.CanWalkRight(this.world, transform)){
                                InputService.Move(input, movement, new Vector2(1, input.Axis.y));
                                patrol.Direction = 1;
                            }else if (GameService.CanWalkRight(this.world, transform)) {
                                InputService.Move(input, movement, new Vector2(1, input.Axis.y));
                                patrol.Direction = 1;
                            }else if (GameService.CanWalkLeft(this.world, transform)) {
                                InputService.Move(input, movement, new Vector2(-1, input.Axis.y));
                                patrol.Direction = -1;
                            }else{
                                InputService.Move(input, movement, new Vector2(0, input.Axis.y));
                                if(this.world.hasComponent(entity, game.Patrol))
                                    this.world.removeComponent(entity, game.Patrol);
                                game.AiInputSystem.ChangeState(this.world, entity, Ai, game.AiState.Idle);
                            }
                            break;
                        case 1:
                            if (GameService.CanWalkRight(this.world, transform)) {
                                InputService.Move(input, movement, new Vector2(1, input.Axis.y));
                                patrol.Direction = 1;
                            } else {
                                InputService.Move(input, movement, new Vector2(0, input.Axis.y));
                                patrol.Direction = 0;
                            }
                            break;
                        case -1:
                            if (GameService.CanWalkLeft(this.world, transform)) {
                                InputService.Move(input, movement, new Vector2(-1, input.Axis.y));
                                patrol.Direction = -1;
                            } else {
                                InputService.Move(input, movement, new Vector2(0, input.Axis.y));
                                patrol.Direction = 0;
                            }
                            break;
                    }
                });
        }
    }
}
