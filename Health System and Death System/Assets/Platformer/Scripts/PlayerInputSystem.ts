
namespace game {
@ut.executeBefore(ut.Shared.InputFence)
    /** New System */
    export class PlayerInputSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Entity, game.PlayerInput, game.Movement], (entity, input, movement) => {
                if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.A)){
                    input.Axis = new Vector2(-1, input.Axis.y);
                }
                else if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.D)){
                    input.Axis = new Vector2(1, input.Axis.y);
                }else{
                    input.Axis = new Vector2(0, input.Axis.y);
                }
                movement.Direction = input.Axis;

                if(ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.Space)){
                    movement.ShouldJump = true;
                }


                if(ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.J)){
                    if(this.world.hasComponent(entity, Attack)){
                        let atk = this.world.getComponentData(entity, game.Attack);
                        atk.ComboStep = true;
                        this.world.setComponentData(entity, atk);
                        return;
                    }
                    let atk = new game.Attack;
                    this.world.addComponentData(entity, atk);
                }


            });
        }
    }
}
