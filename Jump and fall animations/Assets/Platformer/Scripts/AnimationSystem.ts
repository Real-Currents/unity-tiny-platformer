
namespace game {

    @ut.executeAfter(ut.Shared.UserCodeEnd)
    /** New System */
    export class AnimationSystem extends ut.ComponentSystem {

        OnUpdate(): void {
            this.world.forEach([game.Animations, game.Movement], (animations, movement) => {
                if (movement.OnGround) {
                    if (movement.Direction.x == 0) {
                        if (animations.State != 0) {
                            GameService.setEntityEnabled(this.world, animations.Run, false);
                            GameService.setEntityEnabled(this.world, animations.Idle, true);
                            GameService.setEntityEnabled(this.world, animations.Jump, false);
                            animations.State = 0;
                        }
                    } else {
                        if (animations.State != 1) {
                            GameService.setEntityEnabled(this.world, animations.Run, true);
                            GameService.setEntityEnabled(this.world, animations.Idle, false);
                            GameService.setEntityEnabled(this.world, animations.Jump, false);
                            animations.State = 1;
                        }
                    }
                } else {
                    if (animations.State != 2) {
                        let sequencePlayer = this.world.getComponentData(animations.Jump, ut.Core2D.Sprite2DSequencePlayer);
                        sequencePlayer.time = 0;
                        sequencePlayer.paused = false;
                        this.world.setComponentData(animations.Jump, sequencePlayer);

                        GameService.setEntityEnabled(this.world, animations.Run, false);
                        GameService.setEntityEnabled(this.world, animations.Idle, false);
                        GameService.setEntityEnabled(this.world, animations.Jump, true);
                        animations.State = 2;
                    }
                    let sequencePlayer = this.world.getComponentData(animations.Jump, ut.Core2D.Sprite2DSequencePlayer);
                    if (sequencePlayer.time >= 0.8) {
                        sequencePlayer.time = 0.6;
                        sequencePlayer.paused = false;
                        this.world.setComponentData(animations.Jump, sequencePlayer);
                    }

                }
            });
        }
    }
}
