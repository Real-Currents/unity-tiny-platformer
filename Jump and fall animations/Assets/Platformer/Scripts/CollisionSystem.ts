
namespace game {

    /** New System */
    export class CollisionSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([game.Movement, ut.Core2D.TransformLocalPosition], (movement, position) => {
                movement.OnGround = false;
                let _hitResult = ut.HitBox2D.HitBox2DService.hitTest(this.world, position.position.sub(new Vector3(0, 0.05, 0)), GameService.GetCamera(this.world))

                if(!_hitResult.entityHit.isNone()){
                    movement.OnGround =true;
                    movement.Jumping = false;
                }
            });
        }
    }
}
