
namespace game {

    /** New System */
    export class GameService {
       
        private static Camera:ut.Entity;
        static setEntityEnabled(world: ut.World, entity: ut.Entity, enabled: boolean){
            let hasDisabledComponet = world.hasComponent(entity, ut.Disabled);
            if(enabled && hasDisabledComponet){
                world.removeComponent(entity, ut.Disabled);
            }
            else if( !enabled && !hasDisabledComponet){
                world.addComponent(entity, ut.Disabled);
            }
        }

        static GetCamera(world:ut.World):ut.Entity{
            if(this.Camera == null){
                let cameraEntity:ut.Entity;
                world.forEach([ut.Core2D.Camera2D, ut.Entity], (camera, camEntity) => {
                    cameraEntity = new ut.Entity(camEntity.index, camEntity.version);
                });
                this.Camera = cameraEntity;
                return cameraEntity;
            }else{
                return this.Camera;
            }
        }

    }
}
