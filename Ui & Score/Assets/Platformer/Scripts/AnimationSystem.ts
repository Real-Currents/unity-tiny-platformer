
namespace game {

    @ut.executeAfter(ut.Shared.UserCodeEnd)
    /** New System */
    export class AnimationSystem extends ut.ComponentSystem {

        OnUpdate(): void {
            this.world.forEach([ut.Entity, game.Animations, game.Movement], (entity, animations, movement) => {

                if (this.world.hasComponent(entity, game.Attack) || this.world.hasComponent(entity, game.PlayOneShot) || this.world.hasComponent(entity, game.Dying))
                    return;

                if (movement.OnGround) {
                    if (movement.Direction.x == 0) {
                        AnimationSystem.PlayAnimation(this.world, animations, 1);
                    } else {
                        AnimationSystem.PlayAnimation(this.world, animations, 0);
                    }
                } else {
                    AnimationSystem.PlayAnimation(this.world, animations, 2);

                    let sequencePlayer = this.world.getComponentData(animations.Jump, ut.Core2D.Sprite2DSequencePlayer);
                    if (sequencePlayer.time >= 0.8) {
                        sequencePlayer.time = 0.6;
                        sequencePlayer.paused = false;
                        this.world.setComponentData(animations.Jump, sequencePlayer);
                    }

                }
            });

            this.world.forEach([ut.Entity, game.PlayOneShot], (entity, oneShot) => {
                oneShot.TimePlayed += this.scheduler.deltaTime();
                if (oneShot.TimePlayed >= oneShot.Length)
                    this.world.removeComponent(entity, game.PlayOneShot);
            });
        }
        static PlayOneShot(world: ut.World, entity: ut.Entity, length: number, state: number) {
            if (world.hasComponent(entity, game.PlayOneShot)) {
                let oneShot = world.getComponentData(entity, game.PlayOneShot);
                oneShot.TimePlayed = 0;
                oneShot.Length = length;
                world.setComponentData(entity, oneShot);
                let anim = world.getComponentData(entity, game.Animations);
                this.PlayAnimation(world, anim, state, true);
                world.setComponentData(entity, anim);
                return;
            }
            let oneShot = new game.PlayOneShot;
            oneShot.Length = length;
            world.addComponentData(entity, oneShot);
            let anim = world.getComponentData(entity, game.Animations);
            this.PlayAnimation(world, anim, state, true);
            world.setComponentData(entity, anim);
        }
        static PlayAnimation(world: ut.World, animations: game.Animations, state: number, override: boolean = false) {
            if ((override == false && animations.State == state) || animations.State == 7)
                return;
            animations.State = state;
            let animation: ut.Entity = null;
            switch (state) {
                case 0:
                    animation = animations.Run;
                    break;
                case 1:
                    animation = animations.Idle;
                    break;
                case 2:
                    animation = animations.Jump;
                    break;
                case 3:
                    animation = animations.Attack01;
                    break;
                case 4:
                    animation = animations.Attack02;
                    break;
                case 5:
                    animation = animations.Attack03;
                    break;
                case 6:
                    animation = animations.Hurt;
                    break;
                case 7:
                    animation = animations.Dying;
                    break;
            }
            this.ResetAnimationTime(world, animation);

            GameService.setEntityEnabled(world, animations.Run, state == 0);
            GameService.setEntityEnabled(world, animations.Idle, state == 1);
            GameService.setEntityEnabled(world, animations.Jump, state == 2);
            GameService.setEntityEnabled(world, animations.Attack01, state == 3);
            GameService.setEntityEnabled(world, animations.Attack02, state == 4);
            GameService.setEntityEnabled(world, animations.Attack03, state == 5);
            GameService.setEntityEnabled(world, animations.Hurt, state == 6);
            GameService.setEntityEnabled(world, animations.Dying, state == 7);
        }

        static ResetAnimationTime(world: ut.World, animation: ut.Entity) {
            let sequencePlayer = world.getComponentData(animation, ut.Core2D.Sprite2DSequencePlayer);
            sequencePlayer.time = 0;
            sequencePlayer.paused = false;
            world.setComponentData(animation, sequencePlayer);
        }
        static DisableAnimations(world: ut.World, animations: game.Animations): void {
            GameService.setEntityEnabled(world, animations.Run, false);
            GameService.setEntityEnabled(world, animations.Idle, false);
            GameService.setEntityEnabled(world, animations.Jump, false);
            GameService.setEntityEnabled(world, animations.Attack01, false);
            GameService.setEntityEnabled(world, animations.Attack02, false);
            GameService.setEntityEnabled(world, animations.Attack03, false);
            GameService.setEntityEnabled(world, animations.Hurt, false);
            GameService.setEntityEnabled(world, animations.Dying, false);

        }
    }
}
