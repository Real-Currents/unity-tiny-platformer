
namespace game {

    /** New System */
    export class DisplayScoreSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([game.ScoreDisplay, ut.Text.Text2DRenderer], (score, tr) => {
                let scoreValue = game.ScoreService.getScore(this.world).Value.toString();
                if(scoreValue != tr.text)
                    tr.text = scoreValue;
            });
        }
    }
}
