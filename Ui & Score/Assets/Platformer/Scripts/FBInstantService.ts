namespace game {

    export class FBInstantService {

        protected static Instance: FBInstantService = null;
        protected hasInstant: boolean = false;

        public static getInstance(): FBInstantService {
            if (FBInstantService.Instance === null) {
                FBInstantService.Instance = new FBInstantService();
            }
            return FBInstantService.Instance;
        }

        public async initialize() {
            try {
                FBInstant; // this will error out if it's not available
                this.hasInstant = true;
                // let's do our async and goto main menu.
                await FBInstant.initializeAsync();
                FBInstant.setLoadingProgress(100);
                console.log("Starting game async...");
                return await FBInstant.startGameAsync();
            } catch (e) {
                // We don't have this, so let's  just goto the main menu state
                this.hasInstant = false;
                return;
            }
        }
        public async AddHighscore(leaderboardName: string, score: number) {
            return await FBInstant
                .getLeaderboardAsync(leaderboardName)
                .then(leaderboard => {
                    console.log(leaderboard.getName());
                    return leaderboard.setScoreAsync(score, '{race: "elf", level: 3}');
                })
                .then((entry) => console.log(
                    'Score saved' + '\n' +
                    'Score: ' + entry.getScore() + '\n' +
                    'Rank: ' + entry.getRank() + '\n' +
                    'Extra Data: ' + entry.getExtraData()
                ))
                .catch(error => console.error(error));
        }
        public async GetHighscores(leaderboardName: string) {
            return await FBInstant
            .getLeaderboardAsync(leaderboardName)
            .then(leaderboard => leaderboard.getEntriesAsync(10, 0))
            .then(entries => {
              for (var i = 0; i < entries.length; i++) {
                console.log(
                  entries[i].getRank() + '. ' +
                  entries[i].getPlayer().getName() + ': ' +
                  entries[i].getScore()
                );
              }
            }).catch(error => console.error(error));
        }

        public get isAvailable(): boolean {
            return this.hasInstant;
        }
    }


    export enum ContextFilter {
        NEW_CONTEXT_ONLY = "NEW_CONTEXT_ONLY",
        INCLUDE_EXISTING_CHALLENGES = "INCLUDE_EXISTING_CHALLENGES",
        NEW_PLAYERS_ONLY = "NEW_PLAYERS_ONLY",
    }
}
